% Class for CRC

classdef CRC_CLASS
    properties
        B;      % Length of a codeword (b)
        GENERATOR;      % Generator
        G;      % Length of the generator (b)
    end
    
    methods
        % ==================================================Constructor
        function [obj] = CRC_CLASS(A, SELECTED_GENERATOR)
            obj.GENERATOR = SELECTED_GENERATOR;
            obj.G = size(obj.GENERATOR, 2);      % Length of the generator (b)
            obj.B = obj.G + A - 1;      % Length of a codeword (b)
        end
        
        % ==================================================CRC Encoder
        function [CODEWORD] = crc_encoder(obj, DATAWORD)
            A = size(DATAWORD, 2);      % Length of DATAWORD (b)
            AUGMENTED_DATAWORD = [DATAWORD, zeros(1, (obj.B - A))];      % Augment (B - A) bits of 0s to the DATAWORD
            ZERO_GENERATOR = zeros(1, obj.G);

            for i = 1:A
                if AUGMENTED_DATAWORD(i) == 1       % Quotient = 1
                    REMAINDER = xor(AUGMENTED_DATAWORD(i:(i + obj.B - A)), obj.GENERATOR);
                else        % Quotient = 0
                    REMAINDER = xor(AUGMENTED_DATAWORD(i:(i + obj.B - A)), ZERO_GENERATOR);
                end

                AUGMENTED_DATAWORD(i:(i + obj.B - A)) = REMAINDER;      % Inplace Updation
            end

            CODEWORD = [DATAWORD, REMAINDER(2:obj.G)];     % Append the REMAINDER to the DATAWORD
        end
        
        % ==================================================CRC Decoder
        function [decision, DATAWORD] = crc_decoder(obj, MODIFIED_CODEWORD, A)
            MODIFIED_CODEWORD_TEMP = MODIFIED_CODEWORD;
            ZERO_GENERATOR = zeros(1, obj.G);

            for i = 1:A
                if MODIFIED_CODEWORD_TEMP(i) == 1       % Quotient = 1
                    REMAINDER = xor(MODIFIED_CODEWORD_TEMP(i:(i + obj.B - A)), obj.GENERATOR);
                else        % Quotient = 0
                    REMAINDER = xor(MODIFIED_CODEWORD_TEMP(i:(i + obj.B - A)), ZERO_GENERATOR);
                end
                
                MODIFIED_CODEWORD_TEMP(i:(i + obj.B - A)) = REMAINDER;      % Inplace Updation
            end

            SYNDROME = REMAINDER(2:obj.G);

            if any(SYNDROME)        % Discard
                decision = 0;
            else        % Accept
                decision = 1;        
            end

            DATAWORD = MODIFIED_CODEWORD(1:A);
        end
    end
end