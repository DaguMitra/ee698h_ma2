% Introduce Error in the Codeword Randomly
function [MODIFIED_CODEWORD] = introduce_error_randomly(CODEWORD)
    N = size(CODEWORD, 2);      % Length of CODEWORD (b)
    rng('shuffle');
    p = rand;       % Probability
    
    if p <= 0.5     % Modify the CODEWORD
        for i = 1:N
            p = rand;

            if p <= 0.5     % Flip the bit
                CODEWORD(i) = not(CODEWORD(i));
            end
        end
    end

    MODIFIED_CODEWORD = CODEWORD;
end