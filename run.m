clc;

A = 20496;      % Length of a Transport Block (b)
[TRANSPORT_BLOCK] = generate_dataword_randomly(A);
fprintf("Generated Transport Block (%d b) = \n", A);
%disp(TRANSPORT_BLOCK);

% ====================================================================================================Transmit Chain : START
fprintf("\n==========Transmitter==========\n");
% ==================================================CRC Encoding : START
SELECTED_GENERATOR = [];      % Selected Generator
GENERATOR_1 = [1 1 0 0 0 0 1 1 0 0 1 0 0 1 1 0 0 1 1 1 1 1 0 1 1];      % CRC24A as per the standard
%{
GENERATOR_2 = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1];      % CRC24B as per the standard
%GENERATOR_3 = [1 1 0 1 1 0 0 1 0 1 0 1 1 0 0 0 1 0 0 0 1 0 1 1 1];      % CRC24C as per the standard

fprintf("Generator 1 = \n");
disp(GENERATOR_1);
fprintf("Generator 2 = \n");
disp(GENERATOR_2);
fprintf("Generator 3 = \n");
disp(GENERATOR_3);

generator_choice = input('Select a 24 bits generator (1 / 2 / 3 / DEFAULT : 1): ');

switch generator_choice
    case 1
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_1];
    case 2
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_2];
    case 3
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_3];
    otherwise
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_1];
end
%}
SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_1];

crc_obj = CRC_CLASS(A, SELECTED_GENERATOR);      % Object of CRC_CLASS

[EN_TRANSPORT_BLOCK] = crc_obj.crc_encoder(TRANSPORT_BLOCK);
fprintf("Encoded Transport Block (%d b) = \n", crc_obj.B);
%disp(EN_TRANSPORT_BLOCK);
% ==================================================CRC Encoding : END
% ==================================================Segmentation : START
seg_con_obj_tx = SEG_CON_CLASS();      % Object of SEG_CON_CLASS

[seg_con_obj_tx, FILLED_EN_CODE_BLOCKS_MATRIX] = seg_con_obj_tx.segment(crc_obj, EN_TRANSPORT_BLOCK);
% ==================================================Segmentation : END
% ==================================================LDPC Encoding : START
% LDPC Encoder
% ==================================================LDPC Encoding : END
% ====================================================================================================Transmit Chain : END

% ====================================================================================================Channel : START
mat_dim = size(FILLED_EN_CODE_BLOCKS_MATRIX, 1);       % Dimension of FILLED_EN_CODE_BLOCKS_MATRIX
MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX = [];     % Matrix of Modified Filled Encoded Code Blocks

for i=1:mat_dim
    [MODIFIED_FILLED_EN_CODE_BLOCK] = introduce_error_randomly(FILLED_EN_CODE_BLOCKS_MATRIX(i,:));
    fprintf("Received Modified Filled Encoded Code Block = \n");
    %disp(MODIFIED_FILLED_EN_CODE_BLOCK);
    
    MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX = [MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX; MODIFIED_FILLED_EN_CODE_BLOCK];
end

% ====================================================================================================Channel : START

% ====================================================================================================Receive Chain : START
fprintf("\n==========Receiver==========\n");
% ==================================================LDPC Decoding : START
% LDPC Decoder
% ==================================================LDPC Decoding : END
% ==================================================Concatenation : START
seg_con_obj_rx = SEG_CON_CLASS();      % Object of SEG_CON_CLASS for Receiver

[seg_con_obj_rx, EN_TRANSPORT_BLOCK_Rx] = seg_con_obj_rx.concat(A, MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX);
% ==================================================Concatenation : END
% ==================================================CRC Decoding : START
crc_obj_Rx = CRC_CLASS(A, SELECTED_GENERATOR);      % Object of CRC_CLASS
[decision, TRANSPORT_BLOCK_Rx] = crc_obj_Rx.crc_decoder(EN_TRANSPORT_BLOCK_Rx, A);

if decision == 0
    disp("0");
else
    disp("1");
    %fprintf("Received Transport Block = \n");
    %disp(TRANSPORT_BLOCK_Rx);
end
% ==================================================CRC Decoding : END
% ====================================================================================================Receive Chain : END