% Class for Segmentation & Concatenation

classdef SEG_CON_CLASS
    properties
        K_CB = 8448;        % Maximum Length of a Code Block
        GENERATOR_2 = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1];      % CRC24B as per the standard
        L;      % Length of a Code Block CRC
        C;      % Number of Code Blocks
        B_PRIME;        % Length of a Effective Payload
        K_PRIME;        % Minimum Length of a Code Block
        K_b = 22;        % Number of Symmetric Columns in Base Graph 1
        LS_MATRIX = [2 4 8 16 32 64 128 256;
                    3 6 12 24 48 96 192 384;
                    5 10 20 40 80 160 320 0;
                    7 14 28 56 112 224 0 0;
                    9 18 36 72 144 288 0 0;
                    11 22 44 88 176 352 0 0;
                    13 26 52 104 208 0 0 0;
                    15 30 60 120 240 0 0 0];      % Lifting Sizes Matrix
        Z_C = 1;        % Lifting Size
        K;        % Length of a Code Block
        F;      % Length of Filler Bits
    end
    
    methods
        % ==================================================Constructor
        function [obj] = SEG_CON_CLASS()
        end
        
        % ==================================================Segment
        function [seg_con_obj, FILLED_EN_CODE_BLOCKS_MATRIX] = segment(seg_con_obj, crc_obj, EN_TRANSPORT_BLOCK)
            if(crc_obj.B <= seg_con_obj.K_CB)
                seg_con_obj.L = 0;
                seg_con_obj.C = 1;
            else
                seg_con_obj.L = size(seg_con_obj.GENERATOR_2, 2) - 1;
                seg_con_obj.C = ceil(crc_obj.B/(seg_con_obj.K_CB - seg_con_obj.L));
            end            
            
            fprintf("L = ");
            disp(seg_con_obj.L);
            fprintf("C = ");
            disp(seg_con_obj.C);
            seg_con_obj.B_PRIME = crc_obj.B + (seg_con_obj.C * seg_con_obj.L);
            fprintf("B_PRIME = ");
            disp(seg_con_obj.B_PRIME);
            seg_con_obj.K_PRIME = uint64(seg_con_obj.B_PRIME / seg_con_obj.C);
            fprintf("K_PRIME = ");
            disp(seg_con_obj.K_PRIME);
            mat_dim = size(seg_con_obj.LS_MATRIX, 1);       % Dimension of LS_MATRIX

            for i = 1:mat_dim       % Find the Min Z_C
                for j = 1:mat_dim
                    ls = seg_con_obj.LS_MATRIX(i, j);       % Lifting Size

                    if((seg_con_obj.K_b * ls) >= seg_con_obj.K_PRIME)
                        if(seg_con_obj.Z_C == 1)
                            seg_con_obj.Z_C = ls;
                        else
                            if(seg_con_obj.Z_C > ls)
                                seg_con_obj.Z_C = ls;
                            end
                        end
                    end
                end
            end

            fprintf("Z_C = ");
            disp(seg_con_obj.Z_C);
            seg_con_obj.K = seg_con_obj.K_b * seg_con_obj.Z_C;
            fprintf("K = ");
            disp(seg_con_obj.K);
            seg_con_obj.F = seg_con_obj.K - seg_con_obj.K_PRIME;
            fprintf("F = ");
            disp(seg_con_obj.F);
            cb_data_len = seg_con_obj.K_PRIME - seg_con_obj.L;     % Length of Code Block Data
            fprintf("cb_data_len = ");
            disp(cb_data_len);
            crc_obj2 = CRC_CLASS(cb_data_len, seg_con_obj.GENERATOR_2);      % Object of CRC_CLASS
            FILLED_EN_CODE_BLOCKS_MATRIX = [];     % Matrix of Filled Encoded Code Blocks

            for i = 1:seg_con_obj.C
                cb_data = EN_TRANSPORT_BLOCK(((i - 1) * cb_data_len) + 1: (i * cb_data_len));       % Code Block Data
                fprintf("Code Block %d Data = \n", i);
                %disp(cb_data);
                en_cb_data = [];        % Encoded Code Block Data
                
                if(seg_con_obj.C > 1)                    
                    [en_cb_data] = crc_obj2.crc_encoder(cb_data);
                    fprintf("Encoded Code Block %d Data = \n", i);
                    %disp(en_cb_data);
                end
                
                filled_en_cb_data = en_cb_data;     % Filled Encoded Code Block Data
                filled_en_cb_data((seg_con_obj.K_PRIME + 1):seg_con_obj.K) = -1 + zeros(1, seg_con_obj.F);
                fprintf("Filled Encoded Code Block %d Data = \n", i);
                %disp(filled_en_cb_data);
                
                FILLED_EN_CODE_BLOCKS_MATRIX = [FILLED_EN_CODE_BLOCKS_MATRIX; filled_en_cb_data];
            end
        end
        
        % ==================================================Concat
        function [seg_con_obj, EN_TRANSPORT_BLOCK] = concat(seg_con_obj, A, MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX)
            B = A + 24;     % Length of Encoded Transport Block
            
            if(B <= seg_con_obj.K_CB)
                seg_con_obj.L = 0;
                seg_con_obj.C = 1;
            else
                seg_con_obj.L = size(seg_con_obj.GENERATOR_2, 2) - 1;
                seg_con_obj.C = ceil(B/(seg_con_obj.K_CB - seg_con_obj.L));
            end
            
            fprintf("L = ");
            disp(seg_con_obj.L);
            
            fprintf("C = ");
            disp(seg_con_obj.C);
            
            seg_con_obj.B_PRIME = B + (seg_con_obj.C * seg_con_obj.L);
            fprintf("B_PRIME = ");            
            disp(seg_con_obj.B_PRIME);
            
            seg_con_obj.K_PRIME = uint64(seg_con_obj.B_PRIME / seg_con_obj.C);
            fprintf("K_PRIME = ");
            disp(seg_con_obj.K_PRIME);
            
            mat_dim = size(seg_con_obj.LS_MATRIX, 1);       % Dimension of LS_MATRIX

            for i = 1:mat_dim       % Find the Min Z_C
                for j = 1:mat_dim
                    ls = seg_con_obj.LS_MATRIX(i, j);       % Lifting Size

                    if((seg_con_obj.K_b * ls) >= seg_con_obj.K_PRIME)
                        if(seg_con_obj.Z_C == 1)
                            seg_con_obj.Z_C = ls;
                        else
                            if(seg_con_obj.Z_C > ls)
                                seg_con_obj.Z_C = ls;
                            end
                        end
                    end
                end
            end

            fprintf("Z_C = ");
            disp(seg_con_obj.Z_C);
            
            seg_con_obj.K = seg_con_obj.K_b * seg_con_obj.Z_C;
            fprintf("K = ");            
            disp(seg_con_obj.K);
            
            seg_con_obj.F = seg_con_obj.K - seg_con_obj.K_PRIME;
            fprintf("F = ");
            disp(seg_con_obj.F);
            
            cb_data_len = seg_con_obj.K_PRIME - seg_con_obj.L;     % Length of Code Block Data
            fprintf("cb_data_len = ");
            disp(cb_data_len);
            
            crc_obj2 = CRC_CLASS(cb_data_len, seg_con_obj.GENERATOR_2);      % Object of CRC_CLASS
            EN_TRANSPORT_BLOCK = [];     % Encoded Transport Block

            for i = 1:seg_con_obj.C
                en_cb_data = MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX(i,1:seg_con_obj.K_PRIME);       % Encoded Code Block Data                
                fprintf("Encoded Code Block %d Data = \n", i);
                %disp(en_cb_data);
                
                [decision, cb_data] = crc_obj2.crc_decoder(en_cb_data, cb_data_len);      % Code Block Data
                
                if decision == 0
                    disp("0");
                else
                    disp("1");
                    fprintf("Code Block %d Data = \n", i);
                    %disp(cb_data);
                end
                
                EN_TRANSPORT_BLOCK(((i - 1) * cb_data_len) + 1:(i * cb_data_len)) = cb_data;     % Concatenation
            end
        end
    end
end